<?php

/**
 * Retrieve the zone name of the specified zone id.
 */
function bind_get_zone($zid) {
  $zone = db_result(db_query("SELECT name FROM {bind_zones} WHERE id = %d", $zid));
  return $zone;
}

function bind_rndc_status() {
  $cmd = variable_get('bind_rndc', '/usr/sbin/rndc') ." status > /dev/null";
  system($cmd, $status);
  return $status;
}

function bind_rndc_reload() {
  $cmd = variable_get('bind_rndc', '/usr/sbin/rndc') ." reload > /dev/null";
  system($cmd, $status);
  return $status;
}

function bind_namedcheckzone($zid) {
  $zone = bind_get_zone($zid);
  $path = variable_get('bind_zonedata', '/etc/bind/');
  $cmd = variable_get('bind_namedcheckzone', '/usr/sbin/named-checkzone') ." ". $zone ." ". $path . $zone ." > /dev/null";
  system($cmd, $status);
  return $status;
}

function bind_namedcheckconf($zid) {
  $conf = variable_get('bind_zonedata', '/etc/bind/') . variable_get('bind_config', 'drupal_bind.conf');
  $cmd = variable_get('bind_namedcheckconf', '/usr/sbin/named-checkconf') ." ". $conf ." > /dev/null";
  system($cmd, $status);
  return $status;
}

/**
 * Update the serial number for the specified zone.
 */
function bind_update_serial($zid) {
  $serial = db_result(db_query("SELECT serial FROM {bind_zones} WHERE id = %d", $zid));
  $new_serial = date("Ymd") . substr($serial + 1, -2);
  if ($new_serial < $serial) {
    $new_serial = $serial + 1;
  }
  db_query("UPDATE {bind_zones} SET serial = %d WHERE id = %d", $new_serial, $zid);
  return $new_serial;
}

function bind_commit_zones($zid) {
  $email = variable_get('bind_dnsmasteraddress', str_replace('@', '.', variable_get('site_mail', '')));
  $path = variable_get('bind_zonedata', '/etc/bind/');
  $count = db_result(db_query("SELECT COUNT(*) FROM {bind_zones} WHERE updated = '%s'", 'yes'));
  if (!$count) {
    drupal_set_message('No changes have been made. Nothing to commit!');
  }
  $result = db_query("SELECT * FROM {bind_zones} WHERE updated = '%s'", 'yes'); // change * to select each
  while ($zone = db_fetch_array($result)) {
    $output = "\$TTL   ". $zone['ttl'] ."\n@\t IN\t SOA\t". $zone['pri_dns'] .". ". $email .". (\n".
              "\t\t\t" . $zone['serial'] ." \t; Serial\n".
              "\t\t\t" . $zone['refresh'] ." \t\t; Refresh\n".
              "\t\t\t" . $zone['retry'] ." \t\t; Retry\n".
              "\t\t\t" . $zone['expire'] ." \t; Expire\n".
              "\t\t\t" . $zone['ttl'] .")\t\t; Negative Cache TTL\n;\n";
    if ($zone['pri_dns']) {
      $output .= "@\t IN\t NS\t\t". $zone['pri_dns'] .".\n";
    }
    if ($zone['sec_dns']) {
      $output .= "@\t IN\t NS\t\t". $zone['sec_dns'] .".\n";
    }

    $fd = fopen($path . preg_replace('/\//', '-', $zone['name']), "w") or die("Cannot open: ". $path . preg_replace('/\//', '-', $zone['name']));
    fwrite($fd, $output);
    fclose($fd);

    // if header is all ok...
    if (bind_namedcheckzone($zone['id']) == 0) {
      drupal_set_message(t('Zone '. $zone['name'] .' header tested OK (Valid)'));
      db_query("UPDATE {bind_zones} SET updated = 'no', valid = 'yes' WHERE id = %d", $zid);
      $rebuild = "yes";
    }      
    else {
      db_query("UPDATE {bind_zones} SET updated = 'yes', valid = 'no' WHERE id = %d", $zid);
    }

    // this must be changed to insert records after new zone is created
		// problem is how do you insert these without knowing zid before it's created? hmm.
		// fix when making the template functions.
		// if the 'create defaults' is selected in admin...
    if (variable_get('bind_template', '0')) {
      $ip = variable_get('bind_ip', '127.0.0.1');
      // predefined MX
      $output = "\t IN\t MX\t 10\tmail.". $zone['name'] .".\n";
      // @ A
      $output .= "@\t IN\t A\t\t". $ip ."\n";
   	  // mail A
      $output .= "mail\t IN\t A\t\t". $ip ."\n";
			// www @
      $output .= "www\t IN\t CNAME\t\t@\n";
      $fd = fopen($path . preg_replace('/\//', '-', $zone['name']), "a");
      fwrite($fd, $output);
      fclose($fd);
    }

    $records = db_query("SELECT * FROM {bind_records} WHERE zone = %d AND valid != '%s' ORDER BY host, type, pri, destination", $zone['id'], 'no');
    while ($record = db_fetch_array($records)) {
      if ($record['type'] == "MX") {
        $pri = $record['pri'];
      }
      else {
        $pri = "";
      }
    if (( $record['type'] == "NS" || 
          $record['type'] == "PTR" || 
          $record['type'] == "CNAME" ||  
          $record['type'] == "MX" ||  
          $record['type'] == "SRV") && ($record['destination'] != "@")
        ) {
          $destination = $record['destination'] .".";
    }
    elseif ($record['type'] == "TXT") {
      $destination = "\"". $record['destination'] ."\"";
    }
    else {
      $destination = $record['destination'];
    }
		
    $output = $record['host'] ."\t IN\t ". $record['type'] ."\t". $pri ."\t". $destination ."\n";
    $fd = fopen($path . preg_replace('/\//', '-', $zone['name']), "a");
    fwrite($fd, $output);
    fclose($fd);
		
    if (bind_namedcheckzone($zone['id']) == 0) {
      db_query("UPDATE {bind_records} SET valid = '%s' WHERE id = %d", 'yes', $record['id']);
      drupal_set_message(t('bind_namedcheckzone() == 0 OK'));
    }
    else {
      db_query("UPDATE {bind_records} SET valid = '%s' WHERE id = %d", 'no', $record['id']);
      drupal_set_message(t('bind_namedcheckzone() != 0 FAILED '), 'error');
      $fd = fopen($path . preg_replace('/\//', '-', $zone['name']), "r+");
      for ($lines = 0;fgets($fd);$lines++) { $addr[$lines] = ftell($fd); }
      ftruncate($fd, $addr[$lines - 2]);
      fclose($fd);
    }
  }
}

if (isset($rebuild)) {
  $confres = db_query("SELECT name FROM {bind_zones} ORDER BY name");
  $cout = "";
  while ($conf = db_fetch_array($confres)) {
    $cout .= "zone \"". $conf['name'] ."\" {
      type master;
      file \"". $path . $conf['name'] ."\";
      };\n\n";
    }
    $config_file = variable_get('bind_zonedata', '/etc/bind/') . variable_get('bind_config', 'drupal_bind.conf');
    $fd = fopen($config_file, "w");
    fwrite($fd, $cout);
    fclose($fd);
	
    if (bind_namedcheckconf($zid) == 0) {
      drupal_set_message(t('Config file: '. $config_file .' tested OK (Valid)'));
    }
    else {
      die("bind_namedcheckconf() failed");
    }

    if (bind_rndc_reload() == 0) {
      drupal_set_message(t('Bind RNDC Reloaded.'));
    } 
    else {
      die("bind_rndc_reload() failed ");
    }
  } // end while


}